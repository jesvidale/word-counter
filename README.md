# React + Typescript Word Count

## Purpose
This application written in React + Typescript takes a textarea input field and prints a word count of its contents.

### Prototype
The application is a react app that takes a text area input field. The output consists of a line for each word, with the number of its occurrences in the text input. It is also possible to sort the results alphabetically and numerically on the number of occurrences.

The design of the application is a simple bootstrap.

The results are consistent between two text inputs that contain the exact same words in the same frequency but in a different order.

## Assumptions

This project implements a React application written in Typescript that takes a textarea input field and
prints a word count of its contents.

No tests were included whithin this project, because it was not the main focus.

## Important to know!!!

Regex to clean words with some special characters must be improved. There are some breaklines and special characters that should take longer to debug

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

