import { BrowserRouter, Routes, Route, Navigate  } from 'react-router-dom';

import {
  HOME,
  ANY
} from '../constants/paths';

import Counter from './Counter'

const MainRouter = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={HOME} element={<Counter />} />
        <Route path={ANY}>
          <Navigate to={HOME} />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export { MainRouter };
