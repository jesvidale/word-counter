import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { CounterProvider } from './CounterProvider'

import { Entry } from './components/Entry'
import { Detail } from './components/Detail'

const Counter = ():JSX.Element => {
  return (
    <CounterProvider>
      <Container className="p-12">
          <h1 className="header">Words counter app</h1>
          <Row>
            <Col xs={12} md={9}>
              <Entry />
            </Col>
            <Col xs={12} md={3}>
              <Detail />
            </Col>
          </Row>
      </Container>
    </CounterProvider>
  )
}

export { Counter }
