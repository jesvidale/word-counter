import Form from 'react-bootstrap/Form'
import FloatingLabel from 'react-bootstrap/FloatingLabel'

import { useContext } from 'react'
import { CounterContext } from '../../CounterProvider'

const Entry = () => {

  const { wordCount, setWordCount, setWords } = useContext(CounterContext)

  const handleText = (event:any):void => {
    const text = event.target.value
    const words = getWords(text)
    const wordCount = words === [''] ? 0 : words.length
    setWordCount(wordCount)
    setWords(words)
  }

  const getWords = (text:string):string[] => {
    const words = text.toLowerCase().replace(/[.,;:¿?!¡*"()]/g, '').split(' ').filter(el => el !== '')
    return words
  }

  return (
    <>
      <FloatingLabel controlId="counterTextarea" label="Type your text here to recount">
        <Form.Control
          as="textarea"
          placeholder="Leave a comment here"
          style={{ height: '500px', resize: 'none' }}
          onChange={e => handleText(e)}
        />
      </FloatingLabel>
      <span className="d-block mb-4">{wordCount} words</span>
    </>
  )
}

export { Entry }
