import Badge from 'react-bootstrap/Badge'
import ListGroup from 'react-bootstrap/ListGroup'
import Alert from 'react-bootstrap/Alert'
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'

import { useContext, useEffect, useState } from 'react'
import { CounterContext } from '../../CounterProvider'

const Detail = () => {
  const { words } = useContext(CounterContext)
  const [ detail, setDetail ] = useState<IWordElement[] | undefined>()

  interface IWordElement {
    word: string
    count: number
  }
  useEffect(() => {
    setDetail(parseToDetail(words))
  }, [words])

  const parseToDetail = (words: string[]):any => {
    let result: IWordElement[] = []
    words.forEach(word => {
      const repeated = result.length > 0 ? result.find(el => el.word === word): ''
      if (repeated) {
        repeated.count += 1
      } else {
        result.push({
          word: word,
          count: 1
        })
      }
    })
    return result
  }

  const orderDetail = (type:string):void => {
    if (detail !== undefined) {
      const sorted = sortArray(detail, type)
      setDetail([...sorted])
    }
  }

  const sortArray = (arr:IWordElement[], type:string):IWordElement[] => {
    if (type === 'alp') {
      console.log('order asc')
      return arr.sort((a:IWordElement,b:IWordElement) => (a.word > b.word) ? 1 : ((b.word > a.word) ? -1 : 0))
    } else {
      console.log('order type')
      return arr.sort((a:IWordElement,b:IWordElement) => (a.count > b.count) ? 1 : ((b.count > a.count) ? -1 : 0)).reverse()
    }
  }

  const DetailList = ():JSX.Element => {
    if (detail !== undefined && detail.length > 0) {
      return (
        <>
          <DropdownButton id="dropdown-basic-button" title="Order results by  " className="h5 mb-2 d-flex">
            <Dropdown.Item onClick={() => orderDetail('alp')}>Name</Dropdown.Item>
            <Dropdown.Item onClick={() => orderDetail('coi')}>Coincidences</Dropdown.Item>
          </DropdownButton>
          <ListGroup as="ul">
            { detail.map(({word, count}, index: number) => {
              return (
                <ListGroup.Item
                as="li"
                className="d-flex justify-content-between align-items-start"
                key={`${word}${index}`}
              >
                <div className="ms-2 me-auto">
                  <div className="fw-bold">{word}</div>
                </div>
                <Badge bg="primary" pill>{count}</Badge>
              </ListGroup.Item>
              )
            })}
          </ListGroup>
        </>
      )
    } else {
      return (<></>)
    }
  }
  return (
    <>
      <span className="h5 mb-2 d-block">Details</span>
      <Alert key='alert' variant={'info'}>
        {detail?.length} entries found
      </Alert>
      { <DetailList/> }
    </>
  )
}

export { Detail }
