import { ReactNode, createContext } from 'react'
import { useState } from 'react'

interface ICounter {
  words: string[]
  setWords: Function
  wordCount: number
  setWordCount: Function
}

const initialState:ICounter = {
  words: [],
  setWords: () => {},
  wordCount: 0,
  setWordCount: () => {}
}

const CounterContext = createContext(initialState)

interface IProps {
  children: ReactNode
}

const CounterProvider = (props:IProps):JSX.Element =>  {
  const [words, setWords] = useState([])
  const [wordCount, setWordCount] = useState(0)

  const { children } = props
  const value = {words, wordCount, setWords, setWordCount}
  return (
    <CounterContext.Provider
      value={value}
    >{ children }</CounterContext.Provider>
  )
}

export { CounterContext, CounterProvider }
